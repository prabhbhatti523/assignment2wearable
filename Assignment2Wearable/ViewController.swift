//
//  ViewController.swift
//  Assignment2Wearable
//
//  Created by Prabhjinder Singh on 2019-11-01.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    
    
    let USERNAME = "prabhjinderbhatti@gmail.com"
    let PASSWORD = "12345"
    
    let DEVICE_ID = "2e003e001047363333343437"
    var myPhoton : ParticleDevice?
    
    var square = true
    var score = 0
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var answerLabel: UILabel!
    
   var activity1 = true
    
    
    @IBOutlet weak var nextQuestionButton: UIButton!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
     let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        defaults.set(self.score, forKey: "score")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        // Do any additional setup after loading the view.
        self.imageView.image = UIImage(named: "square")!
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                self.subscribeToParticleEvents()
                
            }
            
        } // end getDevice()
    }
    
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let choice = (event?.data)!
                    if(self.activity1 == true){
                    
                    if (choice == "3") {
                        if(self.square == true){
                        self.turnParticleRed()
                            DispatchQueue.main.async {
                                self.answerLabel.text = "answer is not correct"
                            }
                        }
                            
                        else if(self.square == false){
                            self.turnParticleGreen()
                            DispatchQueue.main.async {
                                self.answerLabel.text = "answer is correct"
                            }
                            var score1 =   self.defaults.integer(forKey: "score")
                            
                            self.score = 1 + score1
                            self.defaults.set(self.score, forKey: "score")
                        }
                       
                        
                      
                    }
                    else if (choice == "4") {
                        if(self.square == true){
                            self.turnParticleGreen()
                            print("figure is square")
                            DispatchQueue.main.async {
                                self.answerLabel.text = "answer is correct"
                            }
                            var score1 =   self.defaults.integer(forKey: "score")
                            
                            self.score = 1 + score1
                            self.defaults.set(self.score, forKey: "score")
                        }
                        else if(self.square == false){
                            self.turnParticleRed()
                            DispatchQueue.main.async {
                                self.answerLabel.text = "answer is not correct"
                            }
                        }
                        
                       
                    }
                    
                    else if (choice == "1") {
                        DispatchQueue.main.async {
                            self.scoreLabel.text = " Score: " + String(self.score)
                        }
                        self.showScoreOnParticle()
                        
                        print("score: \(self.score) ")
                    }
                    }
                    else{
                      //  if (choice == "2") {
                            print("Rotate Image 1")
                            print("Rotate Image \(choice)")
                            UIView.animate(withDuration: 2.0, animations: {
                                
                                  DispatchQueue.main.async {
                                    
                                    // error ---Initializer 'init(_:)' requires that 'String' conform to 'BinaryInteger'
                                   // var angle = CGFloat("\(choice)")
                                var angle = CGFloat(Double("\(choice)") ?? 0)
                                self.imageView.transform = CGAffineTransform.identity.rotated(by:angle)
                                    
                                    self.textView.text = "Image rotated by angle \(angle)"
                                    
                            }
                            })

                       // }
                    }
                }
        })
    }
    func showScoreOnParticle(){
        print("show score on particle")
        
        let parameters = ["\(self.score)"]
        var task = myPhoton!.callFunction("showScore", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to show score")
            }
            else {
                print("Error when telling Particle to show score")
            }
        }
    }
    func turnParticleGreen() {
        
        print("Pressed the change lights button")
        
        let parameters = ["green"]
        var task = myPhoton!.callFunction("checkTheAnswer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    
    func turnParticleRed() {
        
        print("Pressed the change lights button")
        
        let parameters = ["red"]
        var task = myPhoton!.callFunction("checkTheAnswer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }


    @IBAction func activity1ButtonPressed(_ sender: Any) {
        self.mainView(input: true)
        self.activity1 = true
        self.nextQuestionButton.isHidden = false
        UIView.animate(withDuration: 2.0, animations: {
            
            DispatchQueue.main.async {
                
                self.imageView.transform = CGAffineTransform(rotationAngle : 0 )
            }
        })

        
        
    }
    @IBAction func activity2ButtonPressed(_ sender: Any) {
        self.mainView(input: false)

        self.activity1 = false
        self.imageView.image = UIImage(named: "triangle")!
        self.nextQuestionButton.isHidden = true
        UIView.animate(withDuration: 2.0, animations: {
            
            DispatchQueue.main.async {
                
                self.imageView.transform = CGAffineTransform(rotationAngle : 0 )
            }
        })
    }
    
    
    @IBAction func randomButtonPressed(_ sender: Any) {
        let randomInt1 = Int.random(in: 1...2)
        if(randomInt1 == 1){
            self.activity1 = true
            self.mainView(input: true)
            self.nextQuestionButton.isHidden = false
        }
        if(randomInt1 == 2){
             self.mainView(input: false)
             self.activity1 = false
            self.nextQuestionButton.isHidden = true
        }
        print(randomInt1)
        
        UIView.animate(withDuration: 2.0, animations: {
            
            DispatchQueue.main.async {
                
                self.imageView.transform = CGAffineTransform(rotationAngle : 0 )
            }
        })
    }
    
    func mainView(input : Bool){
        
        if(input == true){
            textView.text = """
            Identify the number of sides in the given shape.
            Enter your answer using the particle
            1. For 4 sides(Press button 4)
            2. For 3 side(Press button 3)
            3. For next question(Press Next button )
            4. Press button 1 to see score
            """
            
            self.answerLabel.text = "Answer Label"
            self.scoreLabel.text = "Score Label"
            self.imageView.image = UIImage(named: "square")!
            self.square = true
            
        }
        if(input == false){
            textView.text = "Rotate the particle and press button 2 to rotate the shape"
            self.answerLabel.text = ""
            self.scoreLabel.text =  ""
            self.imageView.image = UIImage(named: "triangle")!
            self.square = true
            self.nextQuestionButton.isHidden = true

            
        }
    }
    
    
    
    @IBAction func nextQuestionButton(_ sender: Any) {
        if(self.square == true){
            self.square = false
            self.imageView.image = UIImage(named: "triangle")!
            self.nextQuestionButton.isHidden = false

        }
        else{
            self.square = true
            self.imageView.image = UIImage(named: "square")!
            self.nextQuestionButton.isHidden = false

        }
    }
    
}

