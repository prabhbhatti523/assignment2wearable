// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include <math.h>

InternetButton button = InternetButton();
void setup() {
button.begin();

// Exposed functions
Particle.function("checkTheAnswer", showCorrectOrIncorrect);
Particle.function("showScore", showScore);


}



int DELAY = 200;

void loop() {

// EVENT
if (button.buttonOn(4)) {
Particle.publish("playerChoice","4", 60, PRIVATE);
delay(DELAY);
}
if (button.buttonOn(3)) {
Particle.publish("playerChoice","3", 60, PRIVATE);
delay(DELAY);
}

int xValue = button.readX();


int yValue = button.readY();
int angle = atan2(yValue - 0, xValue - 0);
angle = angle * 180 / M_PI;
// get the angle
char angle1[10];

if (button.buttonOn(2)) {

sprintf(angle1, "%d", angle);
Particle.publish("playerChoice",angle1, 60, PRIVATE);

delay(DELAY);
}
if (button.buttonOn(1)) {
Particle.publish("playerChoice","1", 60, PRIVATE);
delay(DELAY);
}
}
int showCorrectOrIncorrect(String cmd) {
if (cmd == "green") {
button.allLedsOn(0,255,0);
delay(2000);
button.allLedsOff();
}
else if (cmd == "red") {
button.allLedsOn(255,0,0);
delay(2000);
button.allLedsOff();
}
else {
// you received an invalid color, so
// return error code = -1
return -1;
}
// function succesfully finished
return 1;
}

int showScore(String cmd){
int score = cmd.toInt();

int firstLetter = score/10;
int secondLetter = score%10;

if(firstLetter <= 0){
for (int i = 1; i <= secondLetter; i++) {
button.ledOn(i, 255, 255, 0);
}
delay(2000);
button.allLedsOff();
}


else if(firstLetter > 0 && firstLetter <= 5 ){
for (int i = 1; i <= firstLetter; i++) {
button.ledOn(i, 255, 255, 0);
}
delay(2000);
button.allLedsOff();

for (int i = 1; i <= secondLetter; i++) {
button.ledOn(i, 0, 255, 255);
}
delay(2000);
button.allLedsOff();
}

else {
return -1;
}



return 1;
}






















